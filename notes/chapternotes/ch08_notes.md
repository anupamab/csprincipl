## loops

- If we want to print out a count that starts at 1 and increases by 1 each time, we can do easily do that with a for loop. Use the Forward button to see how counter takes on a new value each time through the loop.

- while loop will repeat the body of the loop as long as its logical

## infinative loops

loops forever code

while 1 == 1:
    print("Looping")
    print("Forever")

## counting with a while loop

- loop a spefic times use range

## how to loop when you dont know how to

Start by guessing 2.
Compute the guess squared.
Is the guess squared close to the target number? If it’s within 0.01, we’re done. We’ll take the absolute value of the difference, in case we overshoot. (In Python, abs is the absolute value function.)
If it’s not close enough, we divide the target number by our guess, then average that value with our guess.
That’s our new guess. Square it, and go back to Step 3.

### code 
```
target = 6
guess = 2
guess_squared = guess * guess
while abs(target - guess_squared) > 0.01:
    closer = target / guess
    guess = (guess + closer) / 2.0
    guess_squared = guess * guess
print("Square root of", target, "is", guess)
```

## nested for loops

"The body of any loop, can even include…another loop! Here is a super-simple program that generates all the times tables from 0 to 10. The str() function changes a numeric value into a string."

## inner loop exampls

for x in range(0, 2):
    for y in range(0, 3):
        print('*')

## chapter 8 summary

 Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. The body of a loop is indicated in Python with indention.
Counter - A counter is a variable that is used to count something in a program.
Increment - Increment means to increase the value of a variable by 1.
Infinite Loop - An infinite loop is one that never ends.
Logical Expression - An logical expression is either true or false. An example of a logical expression is x < 3.

def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.
for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
print - The print statement in Python will print the value of the items passed to it.
range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start, stop, step) it returns all the numbers from start to one less than stop changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
while - A while loop is a programming statement that tells the computer to repeat a statement or a set of statements. It repeats the body of the loop while a logical expression is true.

