#  Repeating

## steps
- Use a for loop to repeat code.
- Use range to create a list of numbers
- Loop and Iteration makes things repeat

## numbers
- Indented means that text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary
-  A for loop is one type of loop or way to repeat a statement or set of statements in a program

### example
sum = 0  # Start out with nothing
things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for number in things_to_add:
    sum = sum + number
print(sum)

### question

csp-7-2-1: What is printed if you change the program above so that line 5 is also indented the same amount as line 4?
A. It prints the same thing it did before.
B. It prints the value of sum 10 times and sum is different each time it is printed.
C. It prints the same sum 10 times.
D. You get an error.

it is b

csp-7-2-2: What is printed if you change the program above so that lines 4 and 5 are not indented?
A. It prints the same thing it did before.
B. It prints the value of sum 10 times and sum is different each time it is printed.
C. It prints the same sum 10 times.
D. You get an error.

it is d

## list
- list holds items in a order
- enclosed in [] 
- value sepertaed by commas

# Range function
- range function loops over a sequance of numbers
- If the range function is called with a single positive integer, it will generate all the integer values from 0 to one less than the number it was passed and assign them one at a time to the loop variable


#  Chapter 7 - Summary
## Chapter 7 included the following concepts from computing.

- Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to sum a list of numbers.
- Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. Python uses indention to indicate the body of a loop.
- Indention - Indention means that the text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary of the line. In Python indention is used to specify which statements are in the same block. For example the body of a loop is indented 4 more spaces than the statement starting the loop.
-nIteration - Iteration is the ability to repeat a step or set of steps in a computer program. This is also called looping.
List - A list holds a sequence of items in order. An example of a list in Python is [1, 2, 3].
- Loop - A loop tells the computer to repeat a statement or set of statements.

## Summary of Python Keywords and Functions

- def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.
- for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
- print - The print statement in Python will print the value of the items passed to it.
- range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start,end,step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
