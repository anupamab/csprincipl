# How to get maxuimum points on Create Performance task?

The performance task is 6 points. There are six reporting categorys. The 6 categorys are

1.Program Purpose and Function
2.Data Abstraction
3.Managing Complexity
4.Procedural Abstraction
5.Algorithm Implementation
6.Testing

To get the highest the critira should be completed and done to the best of your ablitys.

## Category 1

In category 1 their are two sections. Their is a video section where you demenstrate the code with a video of the program running. The seconed part is the written response.

**Requirements**

video

- input
- program functionality
- output 
Screenshots and storyboards are not accpted and will not get a point if you dont use a video.

written

- describes the overall purpose of the program.
- describes what functionality of the program is demonstrated in the video.
- describes the input and output of the program demonstrated in the video.

## Category 2

Their is only a written response for this category.

**Requirements**

- includes two program code segments:
  - one that shows how data has been stored in this list (or other collection type).
  - one that shows the data in this same list being used as part of fulfilling the program’s purpose.
- identifies the name of the variable representing the list being used in this response.
- describes what the data contained in this list is representing in the program.
