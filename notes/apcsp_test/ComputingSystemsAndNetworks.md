# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth - maximum amount of data that can be sent in a fixed amount of time. usually measured in bits per second (bps)

- Computing device - a physical device that can run a program. i.e. a phone, tablet, computer, router, or smart senso

- Computing network - a group of interconnected computing devices capable of sending and receiving data

- Computing system - a group of computing devices working together for a common purpose

- Data stream - the method of passing information in the internet which contains chunks of data, which are in packets

- Distributed computing system - the method of making multiple computers work together to solve a common problem

- Fault-tolerant - Fault tolerance is the property that enables a system to continue operating properly in the event of the failure of one or more faults within some of its components

- Hypertext Transfer Protocol (HTTP) - an application layer protocol in the Internet protocol suite model for distributed, collaborative, hypermedia information systems

- Hypertext Transfer Protocol Secure (HTTPS) - he secure version of HTTP, which is the primary protocol used to send data between a web browser and a website

- Internet Protocol (IP) address - a unique address that identifies a device on the internet or a local network

- Packets - a small segment of a larger message

- Parallel computing system - a type of computation in which many calculations or processes are carried out simultaneously

- Protocols - a set of rules the specify the sytems behaviors

- Redundancy - the state of being not or no longer needed or useful

- Router -  a device that connects two or more packet-switched networks or subnetworks
