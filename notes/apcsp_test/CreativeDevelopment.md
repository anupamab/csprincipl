# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment - text in the field of computing, is a segment of a computer file, which consists of object code or the analogous segment of the program's address space that includes executable commands and directives information
- collaboration - the action of working with someone to produce or create something
- comments - a piece of text placed within a program to help other users to understand it, which the computer ignores when running the program
- debugging - identify and remove errors from (computer hardware or software)
- event-driven programming -  a paradigm where entities (objects, services, and so on) communicate indirectly by sending messages to one another through an intermediary
- incremental development process - breaks the software development process down into small, manageable portions known as increments
- iterative development process - a way of breaking down the software development of a large application into smaller chunks
- logic error - occur when there is a fault in the logic or structure of the problem
- overflow error - when the data type used to store data was not large enough to hold the data
- program - write code for (a computer program)
- program behavior - an approach and technique for software development, which enables incremental development in a natural way
- program input - the user giving something to the program
- program output -  any information it processes and sends out
- prototype - A idea of what the actually product is goin to be
- requirements - A set of things that are required for like a project of something
- runtime error - a software or hardware problem that prevents Internet Explorer from working correctly
- syntax error - a character or string incorrectly placed in a command or instruction that causes a failure in execution
- testing - seeing if the program s runnimg correctly without any errors
- user interface - the point of human-computer interaction and communication in a device


## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.

## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
```
for i in range(10)
    print(i)
```
runtime error

Which type of error is this?
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```
overflow error

Which type of error is this?
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```
syntax error
