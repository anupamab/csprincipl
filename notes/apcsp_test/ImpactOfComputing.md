# impact of computing

## main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.

## vocab

- asymmetric ciphers - a process that uses a pair of related keys (public and private) it is used to encrypt and decrypt a message and protect it from unauthorized acces or use

- authentication - making sure the person is actually the person that they are claming to be

- bias - systematic and repeatable errors in a computer system that create unfair outcomes

- Certificate Authority (CA) - a trusted thing thay secures sockets layer (ssl) certificates

- citizen science - a reaserch with the participation of the public

- Creative Commons licensing - a public copyright licenses that can distrubut a otherwise copywrited work

- crowdsourcing - practice of getting information and putting it into a task and put it online

- cybersecurity - security protecting the computer system

- data mining - process of using computer and automation to search large sets of data for patterns and trends

- decryption - turn encryted files back into there original state

- digital divide - unequal access to digital technology, bascicaly the internate and electronics

- encryption - process of encoding information so it hides the true meaning of the informatiom

- intellectual property (IP) - the rights to ones own work

- keylogging - recording the keys struck on a keyboard, spying on what people are typing

- malware - all bad software like virus and other harmful programs that can lock files and steal passwords

- multifactor authentication - multiple diffrent steps to make sure the user is who they really say who they are

- open access - ree access to information and unrestricted use of electronic resources for everyone

- open source - code that is avilble to the public

- free software - software that is free

- FOSS - free and open-source software

- PII (personally identifiable information) - Any representation of information that permits the identity of an individual to whom the information applies to be reasonably inferred by either direct or indirect means

- phishing - hackers trying to trick people into getting sensitive information

- plagiarism - stealing peoples work and ideas and claming its there own

- public key encryption - encrypting data with 2 diffrent keys and making it publicly available

- rogue access point - a device not sanctioned by an administrator, but is operating on the network anyway

- targeted marketing - market for people that will most likely buy the product

- virus - a type of malware that ruins a program
