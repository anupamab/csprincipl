## How is the APCSP exam scored?                                                   
The AP Computer Science Principles exam is scored 1-5, with 5 being a perfect score. The multiple-choice section is worth 70% of your overall grade and is scored by machine. The performance task is worth 30% and scored by a College Board employee.

## How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam? 

I couldnt find how many you have to get right to score a 5,4,3 on the examen but most kids got a 3.

## What is the Create Performance Task and how is it scored?

the Create performance task,is where you will design and implement a program that might solve a problem, enable innovation, explore personal interests, or express creativity.

## On which of the 5 Big Ideas did I score best? 

I scored the best on number 2 the data section.

## On which do I need the most improvement?

I need the most imporvement on number 5 the impacts of computing.

## What online resources are available to help me prepare for the exam in each of the Big Idea areas?

Some  online recources that I can use is the College Board website, Khan Academ and more recources.


