# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- abstraction - the process of removing elements of a code or program that aren't relevant or that distract from more important elements
- analog data - data that is represented in a physical way
- bias - describes systematic and repeatable errors in a computer system that create "unfair" outcomes
- binary number system - A base two system of only using 0 and 1; used by computing devices to represent data digitally
- bit - Short for binary digit, 0 or 1
- byte - 8 bits together = a bite
- classifying data - grouping data with common features and values
- cleaning data - removing curop data and removing or reparing incomplete data and verfiying ranges of dates
- digital data - the electronic representation of information in a format or language that machines can read and understand
- filtering data - identified and extracted to help people make meaning of the data
- information - stimuli that has meaning in some context for its receive
- lossless data compression - Allows the original image to be restored. No data is lost, but the file size cannot be as compressed
- lossy data compression - Lose some data in the compression process. The original can never be restored, but the compression is greater than with lossless techniques.
- metadata - Data that describes data and can help others find the data and use it more effectively.
- overflow error - when the data type used to store data was not large enough to hold the data
- patterns in data - a series of data that repeats in a recognizable way
- round-off or rounding error - a mathematical miscalculation or quantization error caused by altering a number to an integer or one with fewer decimals
- scalability - The ability to increase the capacity of a resource without having to go to a completely new solution, and for that resource to continue to operate at acceptable levels when the increased capacity is being added.
