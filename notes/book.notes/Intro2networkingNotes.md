# Introduction to Networking



## Chapter 1 Introduction

- The things that make todays internet started in the 1960s
- over 20 years of research into how to build internetworking technologies before the first “Internet” was built in the late 1980s by academics in a project called NSFNet

### 1.1 Communicating at a Distance
- converstation in diffrent rooms
- 4 speakers and enoght pieces of wire to connect the microphones
- using microphones and speakers is how a early telephone from the 1900s wpprked

### 1.2 Computers Communicate Differently
- early days of connecting computers it was connected with wires
- in different cities, the leased lines were extended using the longer wires connect- ing the central offices
- if you had enough money, you could lease direct connections between your computers so they could exchange data

### 1.3 Early Wide Area Store-and-Forward Networks
- 1970s - 1980s people working in collage wanted to send people each others data but it cost too much
- few connections you could send data long distances across a patchwork of network connections as long as you were patient

### 1.4 Packets and Routers
- messages to move quickly was to break messages into smaller fragments
- these were called packets
- when messages are broken down into packets they are sent individualy
## vocab

- address: A number that is assigned to a computer so that messages can be routed to the computer.
- hop: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source
computer to its destination.
- LAN: Local Area Network. A network covering an area that is
limited by the ability for an organization to run wires or the power
of a radio transmitter.
- leased line: An “always up” connection that an organization
leased from a telephone company or other utility to send data
across longer distances.
- operator (telephone): A person who works for a telephone company and helps people make telephone calls.
- packet: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the
- Internet. The typical maximum packet size is between 1000 and
3000 characters.
- router: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the best outbound link to speed the packet to its destination.
- store-and-forward network: A network where data is sent
from one computer to another with the message being stored
for relatively long periods of time in an intermediate computer
waiting for an outbound network connection to become available.
WAN: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN
is generally constructed using communication links owned and
managed by a number of different organizations.

# Chapter 2

### four layer tcp/ip modle
- application
- transportation
- internetwork
- link
- link layer responisble for contecting computer to social network

- client: In a networked application, the client application is the
one that requests services or initiates connections.

- fiber optic: A data transmission technology that encodes data using light and sends the light down a very long strand of thin glass or plastic. Fiber optic connections are fast and can cover very long distances.

- offset: The relative position of a packet within an overall mes- sage or stream of data.

- server: In a networked application, the server application is the one that responds to requests for services or waits for incoming connections.

- window size: The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.

# Chapter 3

- lowest layer in internet structure is link layer
- closets to physical network media
- examples:Wired Ether- net, WiFi, and the cellular phone network
- travle from a single link

Mac address into bianary
- 0f - 00110000 01100110
- 2a - 00110010 01100001
- b3 - 01100010 00110011
- 1f - 00110001 01100110
- b3 - 01100010 00110011
- 1a - 00110001 01100001

- mac adress from -r to

#### vocab

- base station: Another word for the first router that handles your packets as they are forwarded to the Internet.

- broadcast: Sending a packet in a way that all the stations con- nected to a local area network will receive the packet.

- gateway: A router that connects a local area network to a wider area network such as the Internet. Computers that want to send data outside the local network must send their packets to the gateway for forwarding.
- MAC Address: An address that is assigned to a piece of network hardware when the device is manufactured.
i- token: A technique to allow many computers to share the same physical media without collisions. Each computer must wait until it has received the token before it can send data.

# Chapter 4
- interwork layer sends data all around the world
- seconed to last layer
- assign another address to every computer based on where the computer is connected to the network
- ip adress broken up into diffrent chunks
- A factory-fresh router barely knows any routes to send things
- when it arrives at a packet that it doesn't know where to send, it asks it's neighbors. If they don't know, they ask theirs, ect. Eventually, the route is found and sent back to the router, who sends it on its way, and stores that info for later use
- most common problem a outbond link fails
- discards the entries in its routing table that were being routed on that link
- Then as more packets arrive for those network numbers, the router goes through the route discovery process again, this time asking all the neighboring routers except the ones that can no longer be contacted due to the broken link.

- core router: A router that is forwarding traffic within the core of
the Internet.
- DHCP: Dynamic Host Configuration Protocol. DHCP is how a portable computer gets an IP address when it is moved to a new location.
- edge router: A router which provides a connection between a
local network and the Internet. Equivalent to “gateway”.
- Host Identifier: The portion of an IP address that is used to identify a computer within a local area network.
- IP Address: A globally assigned address that is assigned to a computer so that it can communicate with other computers that have IP addresses and are connected to the Internet. To simplify routing in the core of the Internet IP addresses are broken into Network Numbers and Host Identifiers. An example IP address might be “212.78.1.25”.
- NAT: Network Address Translation. This technique allows a single global IP address to be shared by many computers on a single local area network.
- Network Number: The portion of an IP address that is used to identify which local network the computer is connected to.
- packet vortex: An error situation where a packet gets into an infinite loop because of errors in routing tables.
- RIR: Regional Internet Registry. The five RIRs roughly correspond to the continents of the world and allocate IP address for the ma- jor geographical areas of the world.
- routing tables: Information maintained by each router that keeps track of which outbound link should be used for each network number.
- Time To Live (TTL): A number that is stored in every packet that is reduced by one as the packet passes through each router. When the TTL reaches zero, the packet is discarded.
- traceroute: A command that is available on many Linux/UNIX systems that attempts to map the path taken by a packet as it moves from its source to its destination. May be called “tracert” on Windows systems.
- two-connected network: A situation where there is at least two possible paths between any pair of nodes in a network. A two- connected network can lose any single link without losing overall connectivity

# Chapter 5
- domain name so you dont have to keep on accesing ip address
ICANN also assigns two-letter country code top-level domain names like .us, .za, .nl, and .jp to countries around the world We call these Country-Code Top-Level Domain Names (ccTLDs). Countries often add second-level TLDs, like .co.uk for commercial organizations within the UK.
- important part making using internet easier
-  DNS: Domain Name System. A system of protocols and servers that allow networked applications to look up domain names and retrieve the corresponding IP address for the domain name
- domain name: A name that is assigned within a top-level do- main. For example, khanacademy.org is a domain that is assigned within the “.org” top-level domain- ICANN: International Corporation for Assigned Network Names and Numbers. Assigns and manages the top-level domains for the Internet
- registrar: A company that can register, sell, and host domain names
- subdomain: A name that is created “below” a domain name. For example, “umich.edu” is a domain name and both “www.umich.edu” and “mail.umich.edu” are subdomains within “umich.edu”
- TLD: Top Level Domain. The rightmost portion of the domain name. Example TLDs include “.com”, “.org”, and “.ru”. Recently, new top-level domains like “.club” and “.help” were added

# Chapter 6 transport layer
- 2 to top layer
- link header is removed when the packet is received on one link and a new link header is added when the packet is sent out on the next link on its journey
- IP and TCP headers stay with a packet as it is going across each link in its journey
- avoid overwhelming network the transport layer only sends a certain amount of data before waiting for an acknowledgement from the Transport layer on the destination computer that the packets were received
- packet lost never arvie at destination
- sending computer holds onto all its data until reciving computer acknolages it
- Transport layer continuously monitors how quickly it receives acknowledgements and dynamically adjusts its window size
- Packet headers have information such as the source and destination IP addresses, the TTL for the packet, and the offset of each packet in the message or file being transmitted
- acknowledgement: When the receiving computer sends a no- tification back to the source computer indicating that data has been received.
- buffering: Temporarily holding on to data that has been sent or received until the computer is sure the data is no longer needed.
- listen: When a server application is started and ready to accept incoming connections from client applications.
- port: A way to allow many different server applications to be waiting for incoming connections on a single computer. Each application listens on a different port. Client applications make connections to well-known port numbers to make sure they are talking to the correct server application.

## Chapter 7 Application Layer

This is the top layer
This is where web browsers, mail programs, and video players operate

### 7.1 Client and Server Applications 
Two parts are required for a networked application to function
The architecture  for networked applications is called client/server
The client portion makes connections to the server application
It retrieves the information to show to the client
Transport layer to exchange data		
When going to a website it connects you to the correct web server looks up the domain name to find the corresponding IP address for the server and makes a transport connection to that IP address, then begins to request data from the server over that network connection. When the data is received, the web browser shows it to you. Sometimes web browsers display a small animated icon to let you know that the data is being retrieved across the network. 
On the other end there is an application called “web server’
It is always waiting for incoming connections

### 7.2 Application Layer Protocols
- Set of rules and protocols
- Protocols describe how a web browser communicates with a web server is described in a number of large documents 					
- the formal name of the protocol between web clients and web servers is the “HyperText Transport Protocol”, or HTTP for short. When you put “http:” or “https:” on the beginning of a URL that you type into the browser, you are indicating that you would like to retrieve a document using the HTTP protocol
So successful because it has client survey protocols

### 7.3 Exploring the HTTP Protocol					
- Internet was created in 1985 by the NSFNet project and the precursor to the NSFNet called the ARPANET was brought up in 1969 				
- Telnet was designed and built even before the first TCP/IP network was in production 

### 7.4 The IMAP Protocol for retrieving Mail
- One of many client/server application protocoles
### 7.5 Flow Control
Figure broken into 6 packets sent but not yet acknowledged and the sending Transport layer. As the Transport layer on the destination computer starts to receive packets, reconstruct the stream of data, and acknowledge packets, it delivers the reconstructed stream of packets to the web browser application display on the user’s screen. Sometimes on a slow connection you can see your browser “paint” pictures as the data is downloaded. On a fast connection the data comes so quickly that the pictures appear instantaneously. 
### 7.6 Writing Networked Applications 				
send and receive data over the network are written in one or more
programming languages good programming library, making a connection to an application running on a server, sending data to the server, and receiving data from the server is generally as easy as reading and writing a file 
### 7.7 Summary
The entire purpose of the lower three layers (Transport, Internet- work, and Link) is to make it so that applications running in the Application layer can focus the application problem that needs to be solved and leave virtually all of the complexity of moving data across a network to be handled by the lower layers of the network model. Because this approach makes it so simple to build networked applications, we have seen a wide range of networked applica- tions including web browsers, mail applications, networked video games, network-based telephony applications, and many others. And what is even more exciting is that it is easy to experiment and build whole new types of networked applications to solve problems that have not yet been imagined. 
### Vocab
- HTML: HyperText Markup Language. A textual format that marks up text using tags surrounded by less-than and greater-than characters. Example HTML looks like: <p>This is <strong>nice</strong></p>.
- HTTP: HyperText Transport Protocol. An Application layer proto- col that allows web browsers to retrieve web documents from web servers. 					
- IMAP: Internet Message Access Protocol. A protocol that allows mail clients to log into and retrieve mail from IMAP-enabled mail servers. 						
flow control: When a sending computer slows down to make sure that it does not overwhelm either the network or the desti- nation computer. Flow control also causes the sending computer to increase the speed at which data is sent when it is sure that the network and destination computer can handle the faster data rates.
- socket: A software library available in many programming lan- guages that makes creating a network connection and exchang- ing data nearly as easy as opening and reading a file on your computer.		
- status code: One aspect of the HTTP protocol that indicates the overall success or failure of a request for a document. The most well-known HTTP status code is “404”, which is how an HTTP server tells an HTTP client (i.e., a browser) that it the requested document could not be found.				
- telnet: A simple client application that makes TCP connections to various address/port combinations and allows typed data to be sent across the connection. In the early days of the Internet, tel- net was used to remotely log in to a computer across the network.		
web browser: A client application that you run on your computer to retrieve and display web pages
- web server: An application that deliver (serves up) Web pages 			
			
	
# secure transport layer				
- networks were small and all of the routers were in secure locations 
2 approaches security 		 	 	 				- first makes sure that all of the network hardware (routers and links) is in physically secure locations so it is not possible for someone to sneak in and monitor traffic while it is crossing the Internet 
- Second, the solution is to encrypt data in your computer before it is sent across its first physical link, and then decrypt the data in the destination computer after it arrives
### 8.1Encrypting and decrypting data
- Protecting data
- Caesar cipher
- Just scramble up the message
- Need to know the number and what correlates to the text
Ex
Plain text:  Go to the rive
Cipher text: Hp up uif sjwfs
### 8.2 Two kinds of secrets
The traditional way to encrypt transmission is by using secret
early days of the internet people could send encrypted emails to each other by the first one calling the other person on the phone to give them the decrption Secret
### 8.3 secure sockets layer
- Add security 20 years after internet protocols
- The solution was a partial layer inbetween the transportation layer and the application layer					
the application requested that the Transport layer make a connection to a remote host, it could request that the connection either be encrypted or unencrypted 
### 8.4 Encrypting Web Browser Traffic 					
“http:” with “https:” to indicate that the browser is to communicate with the web server using the Secure Transport Layer instead of the unencrypted Transport layer				
### 8.5 Certificates and Certificate Authorities 
www.amazon.com and giving you a public key to use for encryp- tion. If your web browser trusts the key, it will use the rogue computer’s public key to encrypt your banking information and send it to the rogue computer. Since the rogue computer gave you the public key, it also has the corresponding private key and is able to decrypt and abscond with your banking information. 

### Vocan

- asymmetric key: An approach to encryption where one (public) key is used to encrypt data prior to transmission and a different (private) key is used to decrypt data once it is received.
- certificate authority: An organization that digitally signs public keys after verifying that the name listed in the public key is actu- ally the person or organization in possession of the public key.
- ciphertext: A scrambled version of a message that cannot be read without knowing the decryption key and technique.
- decrypt: The act of transforming a ciphertext message to a plain text message using a secret or key.
- encrypt: The act of transforming a plain text message to a ci- phertext message using a secret or key.
- plain text: A readable message that is about to be encrypted before being sent
- private key: The portion of a key pair that is used to decrypt transmissions.
- public key: The portion of a key pair that is used to encrypt transmissions.
- shared secret: An approach to encryption that uses the same key for encryption and decryption.
- SSL: Secure Sockets Layer. An approach that allows an appli- cation to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Se-
curity (TLS).
- TLS: Transport Layer Security. An approach that allows an ap- plication to request that a Transport layer connection is to be en- crypted as it crosses the network. Similar to Secure Sockets Layer (SSL).











