# practice problems 3/15/23

khan academy

Q1:Which of these values can be stored in a single bit?

- [ ] 4
- [x] 1
- [ ] [1,0]
- [ ] 10

Q2:Digital alarm clocks display information and visual indicators to help people
wake up on time. Which of the indicators could represent a single bit of information? select two answers

- [ ] The current month (1-12)
- [ ] The current date (1-31)
- [x] The "PM"/"AM" indicator
- [ ] The current hour (1-12)
- [x] The temperature unit indicator ("C" or "F")
 
Q3:How many values can a binary digit store?

- [ ] A binary digit can store one of ten values (0-9).
- [ ] A binary digit can store ten values at a time.
- [X] A binary digit can store one of two values (0 or 1).
- [ ] A binary digit can store two values at a time.

#Presentation 3/21/23

group a

