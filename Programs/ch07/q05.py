product = 1      # Start with the multiplicative identity
numbers = [5,10,15,20,25]
for n in numbers:
    product = product * n
print(product)          # Print the result

