product = 1      # Start with the multiplicative identity
numbers = [1,3,5,7,9,11,13,15,17,19]
for n in numbers:
    product = product * n
print(product)          # Print the result
