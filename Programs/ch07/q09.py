# Step 1: Initialize accumulator
sum = 0   # Start with nothing (addition identity value)

# Step 2: Get data
numbers = [1,2,3,4,5,6,7,8,9,10]

# Step 3: Loop through the data
for number in numbers:
    # Step 4: Accumulate
    sum = sum + number
# Step 5: Display result
print(sum)
