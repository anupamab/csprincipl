# Bitwise Operators by Anupama, Mayah, Blu, and Kiersten

A bitwise operator operates on numbers but insted of the number being treated asa single value, it is treated as it was a string of bits.

# REPL Session 2

```
>>> 3 & 14 
2
>>> 21 | 14 
31
>>> 24 & 12 
8
>>> 24 | 12 
28
>>> 24 ^ 12 
20
>>>
```
## & = AND

1. Conver tthe numbers into binary into bianary
2. Then multiply them
3. put it into decimal form

## | = OR

*  If a 1 is present, the 1 will be shown.
* If there are no 1s, a 0 will be shown

1. convert number into binary
2. then solve

## ^ = XOR
For ^ there is a formula. The formula is  X + X = 0 and X + Y = 1.

1. convert both numbers to bits
2. use formula to solve
3. put binary to decimal
